public without sharing class GetLoginInformation {
   public Static String getUserContactId(){
        /*
        * Method Name: getUserContactId
        * Purpose : get contact Id of Loggedin user.
        * Paramter : null
        * Return : contact Id
        */  
        Id uInfo = UserInfo.getUserId();
        return [SELECT Id,ContactId FROM User WHERE Id =: uInfo ].ContactId;
        
    }
}